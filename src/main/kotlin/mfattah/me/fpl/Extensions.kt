package mfattah.me.fpl

import io.ktor.http.*

fun HttpMessageBuilder.cookie(cookie: String) {
    if (HttpHeaders.Cookie !in headers) {
        headers.append(HttpHeaders.Cookie, cookie)
        return
    }
    headers[HttpHeaders.Cookie] = headers[HttpHeaders.Cookie] + "; " + cookie
}