package mfattah.me.fpl

import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.metrics.micrometer.*
import io.ktor.server.plugins.callloging.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import io.ktor.util.*
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.slf4j.event.Level

@Serializable
data class UserSession(val player: PlayerInfo, val cookies: List<String>) : Principal

@Suppress("unused")
fun Application.configureSession() {
    val secretEncryptKey = hex(this.environment.config.property("keys.encrypt").getString())
    val secretSignKey = hex(this.environment.config.property("keys.signing").getString())

    install(Sessions) {

        cookie<UserSession>("user_session") {
            cookie.path = "/"
            cookie.maxAgeInSeconds = 1209600
            transform(SessionTransportTransformerEncrypt(secretEncryptKey, secretSignKey))
        }
    }
}

@Suppress("unused")
fun Application.configureAuthentication() {

    install(Authentication) {
        form("auth-form") {
            userParamName = "email"
            passwordParamName = "password"

            validate {credentials ->
                val resp = login(credentials.name, credentials.password)
                val cookies = resp.setCookie()
                if(cookies.firstOrNull { it.name == "pl_profile" } != null) {
                    val cookieStrings = cookies.map { renderCookieHeader(it) }
                    val info = userInfo(cookieStrings)
                    UserSession(info.player, cookieStrings)
                } else {
                    null
                }
            }

            skipWhen {
                it.sessions.get<UserSession>()?.cookies != null
            }
        }

        session<UserSession>("auth-session") {
            validate { session -> session }
            challenge {
                call.respondRedirect("/login")
            }
        }
    }
}

@Suppress("unused")
fun Application.configureSerialization() {
    install(ContentNegotiation) {
        json(
            Json {
                ignoreUnknownKeys = true
            }
        )
    }
}

@Suppress("unused")
fun Application.configureMonitoring() {

    val appMicrometerRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

    install(MicrometerMetrics) {
        registry = appMicrometerRegistry
    }
    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    routing {
        get("/metrics-micrometer") {
            call.respond(appMicrometerRegistry.scrape())
        }
    }
}