package mfattah.me.fpl

import io.ktor.server.config.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction


fun main(args: Array<String>) {
    commandLineEnvironment(args).also { initDatabase(it.config) }
    transaction { SchemaUtils.create(ManagersTeam.Leagues, ManagersTeam.LeagueTeams) }
    EngineMain.main(args)
}

fun initDatabase(config: ApplicationConfig) {
    val url = config.property("database.url").getString()
    val driver = config.property("database.driver").getString()
    val username = config.propertyOrNull("database.username")?.getString() ?: ""
    val password = config.propertyOrNull("database.password")?.getString() ?: ""
    TransactionManager.defaultDatabase = Database.connect(url, driver, username, password)
}
