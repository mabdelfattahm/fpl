package mfattah.me.fpl

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.html.*
import io.ktor.server.plugins.autohead.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import kotlinx.html.*
import mfattah.me.fpl.ManagersTeam.Leagues.externalId
import org.jetbrains.exposed.sql.transactions.transaction

@Suppress("unused", "kotlin:S3776")
fun Application.configureRouting() {
    install(AutoHeadResponse)

    routing {
        authenticate("auth-form") {
            post("/api/v1/login") {
                val principal = call.principal<UserSession>()
                if(principal != null) {
                    call.sessions.set(principal)
                    call.respond("/api/v1")
                } else {
                    if(call.sessions.get<UserSession>() != null) {
                        call.sessions.get<UserSession>()?.let { call.respond(it) }
                    } else {
                        call.respond(HttpStatusCode.Unauthorized)
                    }
                }
            }
        }

        get("/api/v1/fpl/{manager_id}/classic-leagues") {
            call.parameters["manager_id"]
                ?.toInt()
                ?.let { call.respond(classicLeagues(it, call.parameters["admin"] == "true")) }
        }

        get("/api/v1/fpl/classic-leagues/{league_id}") {
            call.parameters["league_id"]?.let {
                call.respond(
                    ClassicLeagueTeams(
                        classicLeagueStandings(it.toInt())
                            .standings
                            .results
                    )
                )
            } ?: call.respond(HttpStatusCode.BadRequest)
        }

        get("/api/v1/managers-team-leagues/{league_id}/standings") {
            val leagueId = call.parameters["league_id"]?.toInt()
            if(leagueId == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                call.respond(ManagersTeamLeagueStandings(managersLeagueStandings(leagueId)))
            }
        }

        get("/api/v1/managers-team-leagues/{league_id}") {
            val leagueId = call.parameters["league_id"]?.toInt()
            if(leagueId == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                call.respond(
                    transaction {
                        ManagersTeam.League.findById(leagueId)!!.teams.map {
                            ManagersTeamLeagueTeam(it.id.value, it.name, it.managers)
                        }
                    }
                )
            }
        }

        get("/managers-team-leagues/{league_id}/standings") {
            call.parameters["league_id"]?.toInt()?.let { leagueId ->
                val standings = managersLeagueStandings(leagueId)
                call.respondHtml {
                    head {
                        title {
                            + "League Standings"
                        }
                    }
                    body {
                        table {
                            thead {
                                tr {
                                    td {
                                        + "Team Name"
                                    }
                                    td {
                                        + "Score"
                                    }
                                }
                            }
                            tbody {
                                standings.forEach {
                                    tr {
                                        td {
                                            + it.name
                                        }
                                        td {
                                            + it.score.toString()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } ?: call.respond(HttpStatusCode.BadRequest)
        }

        authenticate("auth-session") {

            get("/api/v1/") {
                call
                    .sessions
                    .get<UserSession>()
                    ?.let { call.respond(it) }
            }

            get("/api/v1/fpl/user-info") {
                call
                    .sessions
                    .get<UserSession>()
                    ?.let { call.respond(it.player) }
                    ?: call.respond(HttpStatusCode.Unauthorized)
            }

            get("/api/v1/fpl/classic-leagues") {
                call
                    .sessions
                    .get<UserSession>()
                    ?.player
                    ?.entry
                    ?.let { call.respond(classicLeagues(it, call.parameters["admin"] == "true")) }
            }

            post("/api/v1/managers-team-leagues") {
                val session = call.sessions.get<UserSession>()!!
                val body = call.receive<CreateManagersTeamLeague>()

                if(isLeagueAdmin(session, body.externalId)) {
                    transaction {
                        ManagersTeam.League.new {
                            name = body.name
                            admin = session.player.entry!!
                            externalId = body.externalId
                        }.id.value
                    }.also { call.respond(HttpStatusCode.Created, ManagersTeamLeagueId(it)) }
                } else {
                    call.respond(HttpStatusCode.BadRequest)
                }

            }

            post("/api/v1/managers-team-leagues/{league_id}/teams") {
                val session = call.sessions.get<UserSession>()!!

                val externalLeagueId = call.parameters["league_id"]
                    ?.toInt()
                    ?.let { transaction { ManagersTeam.League.findById(it)?.externalId } }

                if(externalLeagueId == null) {
                    call.respond(HttpStatusCode.BadRequest)
                } else if (!isLeagueAdmin(session, externalLeagueId)) {
                    call.respond(HttpStatusCode.Unauthorized)
                } else {
                    val body = call.receive<CreateManagersTeamLeagueTeam>()
                    transaction {
                        ManagersTeam.League.find { externalId eq externalLeagueId  }
                            .single()
                            .let {
                                ManagersTeam.LeagueTeam.new {
                                    league = it
                                    name = body.name
                                    managers = body.managers
                                }
                                ManagersTeamLeague(
                                    it.id.value,
                                    it.teams.map { team ->
                                        ManagersTeamLeagueTeam(
                                            team.id.value,
                                            team.name,
                                            team.managers
                                        )
                                    }
                                )
                            }
                    }
                    .also { call.respond(it) }
                }
            }
        }
    }
}

suspend fun isLeagueAdmin(session: UserSession, externalLeagueId: Int): Boolean {
    val leaguesWithAdminCapabilities = session
        .player
        .entry
        ?.let { manager(it) }
        ?.let { manager -> manager.leagues.classic.filter { it.admin == manager.id }.map { it.id } }
        ?: listOf()
    return leaguesWithAdminCapabilities.contains(externalLeagueId)
}

suspend fun classicLeagues(managerId: Int, adminOnly: Boolean = false): List<ClassicLeague> {
    return manager(managerId)
        ?.let { manager ->
            if (adminOnly) {
                manager.leagues.classic.filter { it.admin == manager.id }
            } else {
                manager.leagues.classic
            }
        } ?: listOf()
}

suspend fun managersLeagueStandings(leagueId: Int): List<ManagersTeamLeagueStanding> {
    val (id, teams) = transaction {
        ManagersTeam.League.findById(leagueId)!!.let { Pair(it.externalId, it.teams.toList()) }
    }
    val scores = classicLeagueStandings(id).standings.results.associateBy ({ it.entry }, { it.total })
    return teams.map { t -> ManagersTeamLeagueStanding(t.name, t.managers, t.managers.sumOf { scores[it] ?: 0 }) }
        .sortedByDescending { it.score }
}
