package mfattah.me.fpl

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserInfo(val player: PlayerInfo)

@Serializable
data class PlayerInfo(
    val id: Int,
    val entry: Int?,
    @SerialName("first_name") val firstName: String,
    @SerialName("last_name") val lastName: String,
    val email: String,
    val gender: String,
    @SerialName("date_of_birth")val dateOfBirth: String,
    val dirty: Boolean
)

@Serializable
data class ManagerInfo(val id: Int, val leagues: LeagueInfo)

@Serializable
data class LeagueInfo(val classic: List<ClassicLeague>)

@Serializable
data class ClassicLeague(val id: Int, val name: String, @SerialName("admin_entry") val admin: Int?)

@Serializable
data class ClassicLeagueInfo(val standings: ClassicLeagueStandings)

@Serializable
data class ClassicLeagueStandings(
    @SerialName("has_next") val hasNext: Boolean,
    val page: Int,
    val results: List<ClassicLeagueStanding>
)

@Serializable
data class ClassicLeagueStanding(
    val id: Int,
    val total: Int,
    val entry: Int,
    @SerialName("player_name") val playerName: String,
    @SerialName("entry_name") val entryName: String
 )

@Serializable
data class ClassicLeagueTeams(val teams: List<ClassicLeagueStanding>)

@Serializable
data class CreateManagersTeamLeague(val externalId: Int, val name: String)

@Serializable
data class CreateManagersTeamLeagueTeam(val name: String, val managers: List<Int>) {
    init {
        require(managers.distinct().size == managers.size) { "Managers list cannot contain duplicates" }
    }
}

@Serializable
data class ManagersTeamLeagueId(val id: Int)

@Serializable
data class ManagersTeamLeagueTeam(val id: Int, val name: String, val managers: List<Int>)

@Serializable
data class ManagersTeamLeague(val id: Int, val teams: List<ManagersTeamLeagueTeam>)

@Serializable
data class ManagersTeamLeagueStandings(val standing: List<ManagersTeamLeagueStanding>)

@Serializable
data class ManagersTeamLeagueStanding(val name: String, val managers: List<Int>, val score: Int)