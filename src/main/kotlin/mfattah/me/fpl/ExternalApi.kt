package mfattah.me.fpl

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.java.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.*
import java.net.http.HttpClient


val client = HttpClient(Java) {
    install(ContentNegotiation) {
        json(
            Json {
                ignoreUnknownKeys = true
            }
        )
    }
    install(Logging) { level = LogLevel.INFO }
    install(UserAgent) { agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36" }
    install(DefaultRequest) {
        headers {
            append("sec-fetch-user", "?1")
            append("sec-fetch-site", "same-site")
            append("sec-fetch-mode", "navigate")
            append("sec-fetch-dest", "document")
            append("upgrade-insecure-requests", "1")
            append("authority", "users.premierleague.com")

            append(HttpHeaders.CacheControl, "max-age=0")
            append(HttpHeaders.AcceptLanguage, "en-US,en;q=0.9,he;q=0.8")
            append(HttpHeaders.Origin, "https://fantasy.premierleague.com")
            append(HttpHeaders.Referrer, "https://fantasy.premierleague.com/my-team")
            append(HttpHeaders.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
        }
    }
    engine {
        threadsCount = 8
        pipelining = true
        config { version(HttpClient.Version.HTTP_2) }
    }
}

/**
 * https://github.com/amosbastian/fpl/issues/120
 */
suspend fun login(email: String, password: String) =
    client
        .submitForm(
            url = "https://users.premierleague.com/accounts/login/",
            formParameters = Parameters.build {
                append("login", email)
                append("password", password)
                append("app", "plfpl-web")
                append("redirect_uri", "https://fantasy.premierleague.com/a/login")
            }
        )

suspend fun userInfo(cookies: List<String>) =
    client
        .get("https://fantasy.premierleague.com/api/me/") { cookies.forEach { cookie(it) } }
        .body<UserInfo>()

suspend fun classicLeagueInfo(leagueId: Int): JsonObject? {
    return client
        .get("https://fantasy.premierleague.com/api/leagues-classic/$leagueId/standings/")
        .let {
            if (it.status == HttpStatusCode.NotFound) {
                null
            } else {
                it.body<JsonObject>().jsonObject
            }
        }
        ?.let {
            buildJsonObject {
                it.getValue("league").jsonObject.forEach { (k, v) -> put(k, v) }
                put("last_updated_data", it.getValue("last_updated_data"))
            }
        }
}

suspend fun classicLeagueStandings(
    leagueId: Int,
    maxPages: Int = Int.MAX_VALUE,
    page: Int = 1,
    hasNext: Boolean = true,
    results: MutableList<ClassicLeagueStanding> = mutableListOf()
): ClassicLeagueInfo {
    return if (page > maxPages || !hasNext) {
        ClassicLeagueInfo(ClassicLeagueStandings(false, page - 1, results))
    } else {
        val standings = client
            .get("https://fantasy.premierleague.com/api/leagues-classic/$leagueId/standings/?page_standings=$page")
            .body<ClassicLeagueInfo>()
            .standings
        results.addAll(standings.results)
        classicLeagueStandings(
            leagueId,
            maxPages,
            page + 1,
            standings.hasNext,
            results
        )
    }
}

//suspend fun classicLeagueStandings(
//    leagueId: Int,
//    maxPages: Int = Int.MAX_VALUE,
//    page: Int = 1,
//    hasNext: Boolean = true,
//    results: MutableList<JsonElement> = mutableListOf()
//): JsonObject {
//    return if (page > maxPages || !hasNext) {
//        buildJsonObject { putJsonArray("results") { results.forEach { e -> this.add(e) } } }
//    } else {
//        val standings = client
//            .get("https://fantasy.premierleague.com/api/leagues-classic/$leagueId/standings/?page_standings=$page")
//            .body<JsonObject>()
//            .getValue("standings")
//            .jsonObject
//        results.addAll(standings.getValue("results").jsonArray)
//        classicLeagueStandings(
//            leagueId,
//            maxPages,
//            page + 1,
//            standings.getValue("has_next").jsonPrimitive.boolean,
//            results
//        )
//    }
//}

//@Suppress("unused")
//suspend fun classicLeague(leagueId: Int, maxResults: Int = Int.MAX_VALUE): JsonObject? {
//    val limit = ceil(maxResults / 50.0).toInt() // page size
//    return classicLeagueInfo(leagueId)?.let {
//        buildJsonObject {
//            put("league", it)
//            put("standings", classicLeagueStandings(leagueId, maxPages = limit))
//        }
//    }
//}

suspend fun manager(managerId: Int): ManagerInfo? {
    return client
        .get("https://fantasy.premierleague.com/api/entry/$managerId/")
        .let {
            if (it.status == HttpStatusCode.NotFound) {
                null
            } else {
                it.body<ManagerInfo>()
            }
        }
}

@Suppress("unused")
suspend fun gameWeek(managerId: Int, week: Int): JsonObject? {
    return client
        .get("https://fantasy.premierleague.com/api/entry/$managerId/event/$week/picks/")
        .let {
            if (it.status == HttpStatusCode.NotFound) {
                null
            } else {
                it.body<JsonObject>().jsonObject
            }
        }
}