package mfattah.me.fpl

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object ManagersTeam {

    const val MAXIMUM_NAME_LENGTH = 32

    object Leagues : IntIdTable("leagues","league_id") {
        val name = varchar("league_name", MAXIMUM_NAME_LENGTH).uniqueIndex()
        val admin = integer("league_admin")
        val externalId = integer("external_league_id").uniqueIndex()
    }

    class League(id: EntityID<Int>) : IntEntity(id) {
        companion object : IntEntityClass<League>(Leagues)

        var name by Leagues.name
        var admin by Leagues.admin
        var externalId by Leagues.externalId
        val teams by LeagueTeam referrersOn LeagueTeams.league
    }

    object LeagueTeams : IntIdTable("teams", "team_id") {
        val league = reference("league", Leagues)
        val name = varchar("team_name", MAXIMUM_NAME_LENGTH).uniqueIndex()
        val managers = text("team_managers")
    }

    class LeagueTeam(id: EntityID<Int>) : IntEntity(id) {
        companion object : IntEntityClass<LeagueTeam>(LeagueTeams)

        var league by League referencedOn LeagueTeams.league
        var name by LeagueTeams.name
        private var managersSerialized by LeagueTeams.managers

        var managers: List<Int>
            get() = managersSerialized.substring(1, managersSerialized.length - 1).split(",").map { it.toInt() }
            set(value) {
                managersSerialized = value.joinToString(",", prefix = "[", postfix = "]")
            }
    }
}





