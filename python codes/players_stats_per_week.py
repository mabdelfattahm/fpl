# -*- coding: utf-8 -*-
"""
Created on Sun Jun 26 13:18:25 2022

@author: MahmoudMahgoub
"""
import requests
import json
import numpy as np
import pandas as pd
import datetime
import json

with open("players_pts_perweek_fromr.json") as f:
    data = json.load(f)
all_players = []    
for i in data:
    player_id = i['player_id']
    element = i['element']
    fixture = i['fixture']
    opponent_team = i['opponent_team']
    total_points = i['total_points']
    was_home = i['was_home']
    kickoff_time = i['kickoff_time']
    team_h_score = i['team_h_score']
    team_a_score = i['team_a_score']
    theround = i['round']
    minutes = i['minutes']
    goals_scored = i['goals_scored']    
    assists = i['assists']
    clean_sheets = i['clean_sheets']
    goals_conceded = i['goals_conceded']
    own_goals = i['own_goals']
    penalties_saved = i['penalties_saved']    
    penalties_missed = i['penalties_missed']
    yellow_cards = i['yellow_cards']
    red_cards = i['red_cards']    
    saves = i['saves']    
    bonus = i['bonus']
    bps = i['bps']
    influence = i['influence']
    creativity = i['creativity']
    threat = i['threat']
    ict_index = i['ict_index']    
    value = i['value']
    transfers_balance = i['transfers_balance']
    selected = i['selected']
    transfers_in = i['transfers_in']
    transfers_out = i['transfers_out']
    name = i['name']
    team = i['team']
    pos = i['pos']
    difficulty = i['difficulty']
    opponents = i['opponents']
    venue = i['venue'] 
    team_goals = i['team_goals']    
    opposition_goals = i['opposition_goals']    
    game_id = i['game_id']
   
    individual_stats =[player_id, element, fixture, opponent_team, total_points, was_home, kickoff_time, team_h_score, team_a_score, theround, 
                       minutes, goals_scored, assists, clean_sheets, goals_conceded, own_goals, penalties_saved, penalties_missed, yellow_cards, 
                       red_cards, saves, bonus, bps, influence, creativity, threat, ict_index, value, transfers_balance, selected, transfers_in, 
                       transfers_out, name, team, pos, difficulty, opponents, venue, team_goals, opposition_goals, game_id]
    
    # Append the player array to a 2D array of all players
    all_players.append(individual_stats)
    
all_players = np.array(all_players)
 
dataset = pd.DataFrame({'player_id': all_players[:, 0],
                 'element': all_players[:, 1],
                 'fixture': all_players[:, 2],
                 'opponent_team': all_players[:, 3],
                 'total_points': all_players[:, 4],
                 'was_home': all_players[:, 5],
                 'kickoff_time': all_players[:, 6],
                 'team_h_score': all_players[:, 7],
                 'team_a_score': all_players[:, 8],
                 'round': all_players[:, 9],
                 'minutes': all_players[:, 10],
                 'goals_scored': all_players[:, 11],
                 'assists': all_players[:, 12],
                 'clean_sheets': all_players[:, 13],
                 'goals_conceded': all_players[:, 14],
                 'own_goals': all_players[:, 15],
                 'penalties_saved': all_players[:, 16],
                 'penalties_missed': all_players[:, 17],
                 'yellow_cards': all_players[:, 18],
                 'red_cards': all_players[:, 19],
                 'saves': all_players[:, 20],
                 'bonus': all_players[:, 21],
                 'bps': all_players[:, 22],
                 'influence': all_players[:, 23],
                 'creativity': all_players[:, 24],
                 'threat': all_players[:, 25],
                 'ict_index': all_players[:, 26],
                 'value': all_players[:, 27],
                 'transfers_balance': all_players[:, 28],
                 'selected': all_players[:, 29],
                 'transfers_in': all_players[:, 30],
                 'transfers_out': all_players[:, 31],
                 'name': all_players[:, 32],
                 'team': all_players[:, 33],
                 'pos': all_players[:, 34],
                 'difficulty': all_players[:, 35],
                 'opponents': all_players[:, 36],
                 'venue': all_players[:, 37],
                 'team_goals': all_players[:, 38],
                 'opposition_goals': all_players[:, 39],
                 'game_id': all_players[:, 40]})
# Generate a unique filename based on date
filename = str(datetime.datetime.today().date()) + '_fpl_players_stats_perweek.csv'
# Save the table of data as a CSV
dataset.to_csv(index=False, path_or_buf=filename)