# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 01:35:49 2021

@author: MahmoudMahgoub
"""
import requests
import pandas as pd
import numpy as np
import matplotlib.pylab as plt
url = 'https://fantasy.premierleague.com/api/bootstrap-static/'
r = requests.get(url)
json = r.json()
keys = list(json.keys())
elements_df = pd.DataFrame(json['elements'])
elements_types_df = pd.DataFrame(json['element_types'])
teams_df = pd.DataFrame(json['teams'])
slim_elements_df = elements_df[['second_name','team','element_type','selected_by_percent','now_cost','minutes','transfers_in','value_season','total_points']]
#get my profile
url ='https://fantasy.premierleague.com/api/entry/2929617/'
#url = 'https://fantasy.premierleague.com/api/entry/1122437/event/17/picks/'
r = requests.get(url)
myprofile = r.json()
myClassicLeagues = myprofile['leagues']['classic']#get 3ala el rea2 id 
my_fav_league_id = 629620
url = 'https://fantasy.premierleague.com/api/leagues-classic/'+str(my_fav_league_id)+'/standings'
r = requests.get(url)
my_fav_league = r.json()
my_fav_league_results=my_fav_league['standings']['results']
#keys = ['player_name', 'id']
#friends = {x:my_fav_league_results[x] for x in keys}
#my_dict = {d["player_name"]: d for d in my_fav_league_results}
for elem in my_fav_league_results:
    print (elem['player_name'])
#https://fantasy.premierleague.com/entry/55205/history    
#https://fantasy.premierleague.com/api/leagues-classic/{league_id}/standings
#url = 'https://fantasy.premierleague.com/api/leagues-classic/629620/standings'
omar=[]
mazen=[]
el7we7y=[]
osama=[]
bedo=[]
for i in range(3,25):
    
    url = 'https://fantasy.premierleague.com/api/entry/55205/event/'+str(i)+'/picks/'
    r = requests.get(url)
    json = r.json()
    temp=json['entry_history']['points']
    omar.append(temp)
    url = 'https://fantasy.premierleague.com/api/entry/6493745/event/'+str(i)+'/picks/'
    r = requests.get(url)
    json = r.json()
    temp=json['entry_history']['points']
    mazen.append(temp)
    url = 'https://fantasy.premierleague.com/api/entry/5165899/event/'+str(i)+'/picks/'
    r = requests.get(url)
    json = r.json()
    temp=json['entry_history']['points']
    el7we7y.append(temp)
    url = 'https://fantasy.premierleague.com/api/entry/4267211/event/'+str(i)+'/picks/'
    r = requests.get(url)
    json = r.json()
    temp=json['entry_history']['points']
    osama.append(temp)
    
    url = 'https://fantasy.premierleague.com/api/entry/1122437/event/'+str(i)+'/picks/'
    r = requests.get(url)
    json = r.json()
    temp=json['entry_history']['points']
    bedo.append(temp)
#plt.plot(bedo)
plt.plot(osama,'black',omar,'g', mazen, 'r',el7we7y,'b',bedo,'o')


#my_profile = 