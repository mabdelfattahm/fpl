import requests
import json
import numpy as np
import pandas as pd
import datetime
# Make a get request to get the latest player data from the FPL API
link = "https://fantasy.premierleague.com/api/bootstrap-static/"
response = requests.get(link)
# Convert JSON data to a python object
data = json.loads(response.text)
# Initialize array to hold ALL player data
# This will be a 2D array where each row is a different player
all_players = []
# Loop through each player in the data 
for i in data["elements"]:
    assists = i['assists']
    bonus = i['bonus']
    bps = i['bps']
    chance_of_playing_next_round = i['chance_of_playing_next_round']
    chance_of_playing_this_round = i['chance_of_playing_this_round']
    clean_sheets = i['clean_sheets']
    code = i['code']
    corners_and_indirect_freekicks_order = i ['corners_and_indirect_freekicks_order']
    corners_and_indirect_freekicks_text = i['corners_and_indirect_freekicks_text']
    cost_change_event = i['cost_change_event']
    cost_change_event_fall = i['cost_change_event_fall']
    cost_change_start = i['cost_change_start']
    cost_change_start_fall = i['cost_change_start_fall']
    creativity = i['creativity']
    creativity_rank = i['creativity_rank']
    creativity_rank_type = i['creativity_rank_type']
    direct_freekicks_order =i['direct_freekicks_order']
    direct_freekicks_text =i['direct_freekicks_text']
    dreamteam_count = i['dreamteam_count']
    element_type = i['element_type']
    ep_next = i['ep_next']
    ep_this = i['ep_this']
    event_points = i['event_points']
    first_name = i['first_name']
    form = ['form']
    goals_conceded = i['goals_conceded']
    goals_scored = i['goals_scored']
    ict_index = i['ict_index']
    ict_index_rank =i['ict_index_rank']
    ict_index_rank_type=i['ict_index_rank_type']
    i_d = i['id']
    in_dreamteam = i['in_dreamteam']
    influence = i['influence']
    influence_rank=i['influence_rank']
    influence_rank_type=i['influence_rank_type']
    minutes = i['minutes']
    news = i['news']
    news_added = i['news_added']
    now_cost = i['now_cost']
    own_goals = i['own_goals']
    penalties_missed = i['penalties_missed']
    penalties_order =i['penalties_order']
    penalties_saved = i['penalties_saved']
    penalties_text= i['penalties_text']
    photo = i['photo']
    points_per_game = i['points_per_game']
    red_cards = i['red_cards']
    saves = i['saves']
    second_name = i['second_name']
    selected_by_percent = i['selected_by_percent']
    special = i['special']
    squad_number = i['squad_number']
    status = i['status']
    team = i['team']
    team_code = i['team_code']
    threat = i['threat']
    threat_rank =i['threat_rank']
    threat_rank_type =i['threat_rank_type']
    total_points = i['total_points']
    transfers_in = i['transfers_in']
    transfers_in_event = i['transfers_in_event']
    transfers_out = i['transfers_out']
    transfers_out_event = i['transfers_out_event']
    value_form = i['value_form']
    value_season = i['value_season']
    web_name = i['web_name']
    yellow_cards = i['yellow_cards']
    position = data["element_types"][element_type-1]['singular_name']
    team_name = data["teams"][team-1]['name']

# Create a 1D array of the current players stats
    individual_stats = [assists, bonus, bps, chance_of_playing_next_round, chance_of_playing_this_round, clean_sheets, code,
	corners_and_indirect_freekicks_order, corners_and_indirect_freekicks_text, cost_change_event, cost_change_event_fall,
	cost_change_start, cost_change_start_fall, creativity, creativity_rank, creativity_rank_type, direct_freekicks_order,
	direct_freekicks_text, dreamteam_count, element_type, ep_next, ep_this, event_points, first_name, form, goals_conceded,
	goals_scored, ict_index, ict_index_rank, ict_index_rank_type, i_d, in_dreamteam, influence, influence_rank, 
	influence_rank_type, minutes, news, news_added, now_cost, own_goals, penalties_missed, penalties_order, penalties_saved,
	penalties_text, photo, points_per_game, red_cards, saves, second_name, selected_by_percent, special, squad_number, status,
	team, team_code, threat, threat_rank, threat_rank_type, total_points, transfers_in, transfers_in_event, transfers_out,
	transfers_out_event, value_form, value_season, web_name, yellow_cards,position, team_name]
# Append the player array to a 2D array of all players
    all_players.append(individual_stats)
# Convert the 2D array to a numpy array
all_players = np.array(all_players)
# Convert the numpy array to a pandas dataframe (table)
dataset = pd.DataFrame({'assists': all_players[:, 0], 
                'bonus': all_players[:, 1],
                'bps': all_players[:, 2],
                'chance_of_playing_next_round': all_players[:, 3],
                'chance_of_playing_this_round': all_players[:, 4],
                'clean_sheets': all_players[:, 5],
                'code': all_players[:, 6],
                'corners_and_indirect_freekicks_order': all_players[:, 7],
                'corners_and_indirect_freekicks_text': all_players[:, 8],
                'cost_change_event': all_players[:, 9],
                'cost_change_event_fall': all_players[:, 10],
                'cost_change_start': all_players[:, 11],
                'cost_change_start_fall': all_players[:, 12],
                'creativity': all_players[:, 13],
                'creativity_rank': all_players[:, 14],
                'creativity_rank_type': all_players[:, 15],
                'direct_freekicks_order': all_players[:, 16],
                'direct_freekicks_text': all_players[:, 17],
                'dreamteam_count': all_players[:, 18],
                'element_type': all_players[:, 19],
                'ep_next': all_players[:, 20],
                'ep_this': all_players[:, 21],
                'event_points': all_players[:, 22],
                'first_name': all_players[:, 23],
                'form': all_players[:, 24],
                'goals_conceded': all_players[:, 25],
                'goals_scored': all_players[:, 26],
                'ict_index': all_players[:, 27],
                'ict_index_rank': all_players[:, 28],
                'ict_index_rank_type': all_players[:, 29],
                'i_d': all_players[:, 30],
                'in_dreamteam': all_players[:, 31],
                'influence': all_players[:, 32],
                'influence_rank': all_players[:, 33],
                'influence_rank_type': all_players[:, 34],
                'minutes': all_players[:, 35],
                'news': all_players[:, 36],
                'news_added': all_players[:, 37],
                'now_cost': all_players[:, 38],
                'own_goals': all_players[:, 39],
                'penalties_missed': all_players[:, 40],
                'penalties_order': all_players[:, 41],
                'penalties_saved': all_players[:, 42],
                'penalties_text': all_players[:, 43],
                'photo': all_players[:, 44],
                'points_per_game': all_players[:, 45],
                'red_cards': all_players[:, 46],
                'saves': all_players[:, 47],
                'second_name': all_players[:, 48],
                'selected_by_percent': all_players[:, 49],
                'special': all_players[:, 50],
                'squad_number': all_players[:, 51],
                'status': all_players[:, 52],
                'team': all_players[:, 53],
                'team_code': all_players[:, 54],
                'threat': all_players[:, 55],
                'threat_rank': all_players[:, 56],
                'threat_rank_type': all_players[:, 57],
                'total_points': all_players[:, 58],
                'transfers_in': all_players[:, 59],
                'transfers_in_event': all_players[:, 60],
                'transfers_out': all_players[:, 61],
                'transfers_out_event': all_players[:, 62],
                'value_form': all_players[:, 63],
                'value_season': all_players[:, 64],
                'web_name': all_players[:, 65],
                'yellow_cards': all_players[:, 66],
                'position': all_players[:,67],
                'team_name': all_players[:, 68]})
# Generate a unique filename based on date
filename = str(datetime.datetime.today().date()) + '_fpl_players_updated_cols_withpos&teamname.csv'
# Save the table of data as a CSV
dataset.to_csv(index=False, path_or_buf=filename)